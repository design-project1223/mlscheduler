front = -1
rear = 0
q = [0] * 100


def dequeue(front, rear):
    item = q[front]
    front = front + 1
    if (front == rear):
        front = -1
        rear = 0
    return item


def enqueue(front, rear, n):
    q[rear] = n
    rear = rear + 1
    if (front == -1):
        front = front + 1


if __name__ == "__main__":  # Driver code

    processes = [1, 2, 3, 4, 5]  # Process id's
    n = 5
    bt = [50, 120, 90, 250, 290]  # Burst time of all processes
    at = [0, 5, 120, 240, 300]  # Arrival time of all processes
    wt = [0] * n
    rspt = [0] * n
    tat = [0] * n
    status = [0] * n  # status of process in queue
    left = [0] * n  # burst time left of each process
    stproc = [0] * n  # starting time of each process
    statproc = [0] * n  # status of process in gantt chart
    respt = [0] * n
    thpt = 0
    st = [0] * 100
    ct = [0] * 100
    ls = 0
    i=0
    num = 0
    idle = 0

    n = int(input("Enter the number of processes : "))
    tq = int(input("Enter the time quantum : "))
    for i in range(n):
        # processes[i] = input("Process Name:" )
        # at[i] = input("Arrival Time : ")
        # bt[i] = input(Burst Time : ")
        status[i] = 0
        left[i] = bt[i]
    i = 0  # current time

    while ls < n:
        for j in range(n):
            if status[j] == 0 and at[j] <= i:
                enqueue(front, rear, j)
                status[j] = 1

        if (idle == 0 and front == -1):

            st[num] = i
            i = i + 1
            idle = 1
        elif (front != -1):
            if (idle == 1):
                ct[num] = i
                idle = 0
                num = num + 1
            k = dequeue(front, rear)
            st[num] = i
            if (statproc[k] == 0):
                stproc[k] = i
                statproc[k] = 1
            if (left[k] <= tq):
                ct[num] = i + left[k]
                i = ct[num]
                tat[k] = i - at[k]
                wt[k] = tat[k] - bt[k]
                ls = ls + 1
                num = num + 1
                status[k] = 2
            else:
                ct[num] = i + tq
                i = i + tq
                left[k] = left[k] - tq
                num = num + 1
                for j in range(n):
                    if (status[j] == 0 and at[j] <= i):
                        enqueue(front, rear, j)
                        status[j] = 1
                    enqueue(front, rear, k)

        else:
            i = i + 1
    for i in range(n):
        rspt[i] = stproc[i] - at[i]

    print("Process Name\tArrival Time\tTurnaround Time\tWaiting Time\tResponse Time   ")
    for i in range(n):
        print(processes[i], "\t", at[i], "\t", tat[i], "\t", wt[i], "\t", rspt[i])

    for i in range(n):
        thpt = thpt + bt[i]
    print("Throughput :", thpt / n)


















"""#include<stdio.h>
#include<string.h>

struct process {
	char name[20];
	int arrivalTime, burstTime;
	int waitingTime, turnaroundTime;
	int status, left;
} proc[20];

struct done {
	char name[20];
	int startingTime, completionTime;
} d[20];

int q[100], front=-1, rear=0;

void enqueue(int n){
	q[rear++] = n;
	if (front == -1) {
		front++;
	}
}

int dequeue() {
	int item = q[front++];
	if (front == rear) {
		front = -1;
		rear  = 0;
	}
	return item;
}

void main() {

	int n, t, ls=0, i=0, j, k, totalTurnaroundTime=0, totalWaitingTime=0, num=0, idle=0;
	
	printf("Enter the number of processes : ");
	scanf("%d",&n);
	printf("Enter the Time Quantum        : ");
	scanf("%d",&t);
	

	for ( i=0; i<n; ++i) {
		printf("\nProcess Name : ");
		scanf("%s",proc[i].name);
		printf("Arrival Time : ");
		scanf("%d",&proc[i].arrivalTime);
		printf("Burst Time   : ");
		scanf("%d",&proc[i].burstTime);
		proc[i].status = 0;
		proc[i].left = proc[i].burstTime;
	}
	
	i=0;
	while ( ls < n ) {
		for (j=0;j<n; ++j) {
			if (proc[j].status == 0 && proc[j].arrivalTime<=i) {
				enqueue(j);
				proc[j].status = 1;
			}
		}
		if (idle == 0 && front == -1) {
			strcpy(d[num].name,"IDLE");
			d[num].startingTime = i;
			++i;
			idle = 1;
		} else if (front != -1) {
			if (idle == 1) {
				d[num].completionTime = i;
				idle = 0;
				num++;
			}
			k = dequeue();
			d[num].startingTime = i;
			strcpy(d[num].name,proc[k].name);
			if (proc[k].left <= t) {
				d[num].completionTime = i + proc[k].left;
				i = d[num].completionTime;
				proc[k].turnaroundTime = i - proc[k].arrivalTime;
				proc[k].waitingTime = proc[k].turnaroundTime - proc[k].burstTime;
				ls++;
				num++;
				proc[k].status = 2;
			} else {
				d[num].completionTime = i + t;
				i = i + t;
				proc[k].left = proc[k].left - t;
				num++;
				for (j=0; j<n; ++j) {
					if (proc[j].status == 0 && proc[j].arrivalTime<=i) {
						enqueue(j);
						proc[j].status = 1;
					}
				}
				enqueue(k);
			}
		} else {
			++i;
		}
	}


	printf("\nProcess Name     Turnaround Time    Waiting Time\n");
	for ( j=0; j<n; ++j) {
		printf("%s                %d                  %d\n",proc[j].name,proc[j].turnaroundTime,proc[j].waitingTime);
	}
	

	for (j=0; j<n; ++j) {
		totalTurnaroundTime = totalTurnaroundTime + proc[j].turnaroundTime;
		totalWaitingTime = totalWaitingTime + proc[j].waitingTime;
	}
	printf("\nAverage Turnaround Time = %.2fms",(float)totalTurnaroundTime/n);
	printf("\nAverage Waiting Time    = %.2fms\n",(float)totalWaitingTime/n);
	

	printf("\n-");
	for (j=0; j<num; ++j) {
		printf("--");
		for (k=0; k<strlen(d[j].name);++k) {
			printf("-");
		}
		if (d[j].completionTime>9) {
			printf("----");
		} else {
			printf("---");
		}
	}
	printf("\n|");
	for (j=0; j<num; ++j) {
		if (d[j].completionTime>9){
			printf("  %s   |",d[j].name);
		} else {
			printf("  %s  |",d[j].name);
		}
	}
	printf("\n-");
	for (j=0; j<num; ++j) {
		printf("--");
		for (k=0; k<strlen(d[j].name);++k) {
			printf("-");
		}
		if (d[j].completionTime>9) {
			printf("----");
		} else {
			printf("---");
		}
	}
	printf("\n0");
	for (j=0; j<num; ++j) {
		printf("  ");
		for (k=0; k<strlen(d[j].name);++k) {
			printf(" ");
		}
		printf("  %d",d[j].completionTime);
	}
	printf("\n");
}
"""

 
              
















