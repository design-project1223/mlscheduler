#include <ESP8266WiFi.h>

String  i;
WiFiServer server(80);
int red =10;
int yellow =9;
int green=8;

void setup()
{
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  i = "";
  Serial.begin(9600);
  WiFi.disconnect();
  delay(3000);
  WiFi.begin("xenomech101");
  while ((!(WiFi.status() == WL_CONNECTED))){
    delay(300);
    Serial.println(".........");
  }
  Serial.println("WiFi Connected!");
  Serial.println((WiFi.localIP().toString()));
  server.begin();

}
void loop()
{
    WiFiClient client = server.available();
    if (!client) { return; }

    while(!client.available()){  delay(1); }

    i = (client.readStringUntil('\r'));
    i.remove(0, 5);
    i.remove(i.length()-9,9);

    if (i == "ON") {
     
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("");
      client.println("<!DOCTYPE HTML>");
      client.println("<html>");
      client.println("Buzzing");
      client.println("</html>");
      changeLights();
      delay(15000);
      client.stop();
      delay(1);
    }

    if (i == "OFF") {
      digitalWrite(0,LOW);
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("");
      client.println("<!DOCTYPE HTML>");
      client.println("<html>");
      client.println("Stoped Buzzing");
      client.println("</html>");
      client.stop();
      delay(1);

    }
}

void changeLights()
{
  
  digitalWrite(green,LOW);
  digitalWrite(yellow,HIGH);
  delay(1500);

  
  
  digitalWrite(yellow,LOW);
  digitalWrite(red,HIGH);
  delay(1500);
  
 
  digitalWrite(red,LOW);
  delay(1500);

  
  digitalWrite(yellow,LOW);
  digitalWrite(green,HIGH);
  delay(1500);
 
}
