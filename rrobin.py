def findWaitingTime(processes, n, bt,st, wt, quantum):
    rem_bt = [0] * n


    for i in range(n):
        rem_bt[i] = bt[i]                            #remaining burst time
    t = 0                                            # Current time


    while (1):
        done = True
        for i in range(n):


            if (rem_bt[i] > 0):
                done = False                        # There is a pending process
                if(rem_bt[i]== bt[i]):
                    st[i] = t

                if (rem_bt[i] > quantum):


                    t += quantum

                    rem_bt[i] -= quantum            # Decrease the burst_time of current process by quantum

                else:


                     t = t + rem_bt[i]

                     wt[i] = t - bt[i]                 # Waiting time is current time minus time used by this process

                     rem_bt[i] = 0                     # As the process gets fully executed make its remaining burst time = 0


        if (done == True):                             # If all processes are done
            break






# Driver code
if __name__ == "__main__":

    total_time = 0
    processes = [1, 2, 3, 4, 5, 6]  # Process id's
    n = 6
    at= [0,1,2,3,4,6]
    bt = [4,5,2,1,6,3]          # Burst time of all processes
    for i in range(n):                   # Display processes along with all details
        total_time += bt[i]

    print ("Enter The Time Quantum: ")
    quantum = int(input())
    wt = [0] * n
    st = [0] * n
    tat = [0] * n
    rspt= [0] * n

    findWaitingTime(processes, n, bt,st,wt, quantum) # Function to find waiting  time for all processes
    for i in range(n):
        rspt[i] = st[i] - at[i]

    print("Processes Arrival Time Burst Time  Waiting Time Response Time") # Display processes along with all details

    for i in range(n):

        print( " ", i + 1 , "\t\t" ,at[i], "\t\t\t" , bt[i] , "\t\t\t " , wt[i] , "\t\t\t\t" , rspt[i])

    print("\nThroughput is:" ,(total_time / n))

