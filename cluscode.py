import sys, os, re
import logging
import numpy as np
from collections import defaultdict
import cluster_simulation_protos_pb2

logging.basicConfig(level=logging.DEBUG)

def usage():
  print "usage: generate-txt-from-protobuff.py <input_protobuff_name> <optional: base name for output files. (defaults to inputfilename)>"
  sys.exit(1)

logging.debug("len(sys.argv): " + str(len(sys.argv)))

if len(sys.argv) < 2:
  logging.error("Not enough arguments provided.")
  usage()

try:
  input_protobuff_name = sys.argv[1]
  # Start optional args.
  if len(sys.argv) == 3:
    outfile_name_base = str(sys.argv[2])
  else:
    #make the output files the same as the input but add .txt to end
    outfile_name_base = input_protobuff_name

except:
  usage()

logging.info("Input file: %s" % input_protobuff_name)

def get_mad(median, data):
  logging.info("in get_mad, with median %f, data: %s"
               % (median, " ".join([str(i) for i in data])))
  devs = [abs(x - median) for x in data]
  mad = np.median(devs)
  print "returning mad = %f" % mad
  return mad

# Read in the ExperimentResultSet.
experiment_result_set = cluster_simulation_protos_pb2.ExperimentResultSet()
infile = open(input_protobuff_name, "rb")
experiment_result_set.ParseFromString(infile.read())
infile.close()

# This dictionary, indexed by 3tuples[String] of
# (cell_name, scheduler_name, metric_name), holds as values strings
# each holding all of the rows that will be written to to a text file
# uniquely identified by the dictionary key.
# This dictionary will be iterated over after being being filled
# to create text files holding its contents.
output_strings = defaultdict(str)
# Loop through each experiment environment.
logging.debug("Processing %d experiment envs."
              % len(experiment_result_set.experiment_env))
for env in experiment_result_set.experiment_env:
  logging.debug("Handling experiment env (%s %s)."
                % (env.cell_name, env.workload_split_type))
  logging.debug("Processing %d experiment results."
                % len(env.experiment_result))
  prev_l_val = -1.0
  for exp_result in env.experiment_result:
    logging.debug("Handling experiment result with C = %f and L = %f."
                  % (exp_result.constant_think_time,
                     exp_result.per_task_think_time))
    for sched_stat in exp_result.scheduler_stats:
      logging.debug("Handling scheduler stat for %s."
                    % sched_stat.scheduler_name)
      # Calculate per day busy time and conflict fractions.
      daily_busy_fractions = []
      daily_conflict_fractions = []
      for day_stats in sched_stat.per_day_stats:
        # Calculate the total busy time for each of the days and then
        # take median of all fo them.
        run_time_for_day = env.run_time - 86400 * day_stats.day_num
        logging.info("setting run_time_for_day = env.run_time - 86400 * "
                     "day_stats.day_num = %f - 86400 * %d = %f"
                     % (env.run_time, day_stats.day_num, run_time_for_day))
        if run_time_for_day > 0.0:
          daily_busy_fractions.append(((day_stats.useful_busy_time +
                                        day_stats.wasted_busy_time) /
                                       min(86400.0, run_time_for_day)))
          logging.info("%s appending daily_conflict_fraction %f."
                       % (sched_stat.scheduler_name, daily_busy_fractions[-1]))

          if day_stats.num_successful_transactions > 0:
            conflict_fraction = (float(day_stats.num_failed_transactions) /
                                 float(day_stats.num_failed_transactions +
                                       day_stats.num_successful_transactions))
            daily_conflict_fractions.append(conflict_fraction)
            logging.info("%s appending daily_conflict_fraction %f."
                         % (sched_stat.scheduler_name, conflict_fraction))
          else:
            daily_conflict_fractions.append(0)
            logging.info("appending 0 to daily_conflict_fraction")

      logging.info("Done building daily_busy_fractions: %s"
                   % " ".join([str(i) for i in daily_busy_fractions]))
      logging.info("Also done building daily_conflict_fractions: %s"
                   % " ".join([str(i) for i in daily_conflict_fractions]))

      if prev_l_val != exp_result.per_task_think_time and prev_l_val != -1.0:
        opt_extra_newline = "\n"
      else:
        opt_extra_newline = ""
      prev_l_val = exp_result.per_task_think_time

      # Compute the busy_time row and append it to the string
      # accumulating output rows for this schedulerName.
      daily_busy_fraction_median = np.median(daily_busy_fractions)
      busy_frac_key = (env.cell_name, sched_stat.scheduler_name, "busy_frac")
      output_strings[busy_frac_key] += \
          "%s%s %s %s %s %s %s %s\n" % (opt_extra_newline,
                                      env.cell_name,
                                      sched_stat.scheduler_name,
                                      exp_result.constant_think_time,
                                      exp_result.per_task_think_time,
                                      exp_result.avg_job_interarrival_time,
                                      daily_busy_fraction_median,
                                      get_mad(daily_busy_fraction_median,
                                              daily_busy_fractions))

      conflict_fraction_median = np.median(daily_conflict_fractions)
      conf_frac_key = (env.cell_name, sched_stat.scheduler_name, "conf_frac")
      output_strings[conf_frac_key] += \
          "%s%s %s %s %s %s %s %s\n" % (opt_extra_newline,
                                     env.cell_name,
                                     sched_stat.scheduler_name,
                                     exp_result.constant_think_time,
                                     exp_result.per_task_think_time,
                                     exp_result.avg_job_interarrival_time,
                                     conflict_fraction_median,
                                     get_mad(conflict_fraction_median,
                                             daily_conflict_fractions))

# Create output files.
# One output file for each unique (cell_name, scheduler_name, metric) tuple.
for key_tuple, out_str in output_strings.iteritems():
  outfile_name = (outfile_name_base +
                  "." + "_".join([str(i) for i in key_tuple]) + ".txt")
  logging.info("Creating output file: %s" % outfile_name)
  outfile = open(outfile_name, "w")
  outfile.write(out_str)
  outfile.close()
from matplotlib import use, rc
use('Agg')
import matplotlib.pyplot as plt

# plot saving utility function
def writeout(filename_base, formats = ['pdf']):
  for fmt in formats:
    plt.savefig("%s.%s" % (filename_base, fmt), format=fmt, bbox_inches='tight')
#    plt.savefig("%s.%s" % (filename_base, fmt), format=fmt)

def set_leg_fontsize(size):
  rc('legend', fontsize=size)

def set_paper_rcs():
  rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],
               'serif':['Helvetica'],'size':8})
  rc('text', usetex=True)
  rc('legend', fontsize=7)
  rc('figure', figsize=(3.33,2.22))
#  rc('figure.subplot', left=0.10, top=0.90, bottom=0.12, right=0.95)
  rc('axes', linewidth=0.5)
  rc('lines', linewidth=0.5)

def set_rcs():
  rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],
               'serif':['Times'],'size':12})
  rc('text', usetex=True)
  rc('legend', fontsize=7)
  rc('figure', figsize=(6,4))
  rc('figure.subplot', left=0.10, top=0.90, bottom=0.12, right=0.95)
  rc('axes', linewidth=0.5)
  rc('lines', linewidth=0.5)

def append_or_create(d, i, e):
  if not i in d:
    d[i] = [e]
  else:
    d[i].append(e)

# Append e to the array at position (i,k).
# d - a dictionary of dictionaries of arrays, essentially a 2d dictionary.
# i, k - essentially a 2 element tuple to use as the key into this 2d dict.
# e - the value to add to the array indexed by key (i,k).
def append_or_create_2d(d, i, k, e):
  if not i in d:
    d[i] = {k : [e]}
  elif k not in d[i]: 
    d[i][k] = [e]
  else:
    d[i][k].append(e)

def cell_to_anon(cell):
  if cell == 'A':
    return 'A'
  elif cell == 'B':
    return 'B'
  elif cell == 'C':
    return 'C'
  elif cell == 'synth':
    return 'SYNTH'
  else:
    print "unknown cell!?"
    raise Exception
