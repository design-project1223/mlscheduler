from operator import itemgetter
machine_list =[
    {"processor": "i5","available": 6 ,"alloted": 0 ," memory per core " : 8 , "num core" : 4 , "flops " : 354 , "process" : "" , "allocated list " : [ ] , "idle" : 0 , "current": 0 , "time" : [ 0 ] } ,
{"processor " : "i3" , " available" : 6 , "alloted " : 0 , "memory per core" : 4 , "num core" : 2 , "flops" : 289 , "process" : "" , " allocated list " : [ ] , "idle " : 0 , " current " : 0 , "time" : [ 0 ] } ,
{ "processor" : "Dualcore" , "available": 6 , "alloted": 0 , " memory per core " : 2 , "num core": 2 , "flops" : 105 , "process" : "" , "allocated list": [ ] , "idle" : 0 , "current" : 0 , "time" : [ 0 ] }
]
input_list = [
    {"processno": 1, "cores needed": 12, "arrival": 0, "burstime": 50, "priority": 5, "i5": 6, "i3": 12, "dualcore": 12,
     "st": 0, "ct": 0},
    {"processno": 2, "cores needed": 12, "arrival": 5, "burstime": 120, "priority": 2, "i5": 6, "i3": 12,
     "dualcore": 12, "st": 0, "ct": 0},
    {"processno": 3, "cores needed": 23, "arrival": 120, "burstime": 90, "priority": 4, "i5": 6, "i3": 12,
     "dualcore": 12, "st": 0, "ct": 0},
    {"processno": 4, "cores needed": 12, "arrival": 240, "burstime": 250, "priority": 1, "i5": 2, "i3": 4,
     "dualcore": 4, "st": 0, "ct": 0},
    {"processno": 5, "cores needed": 10, "arrival": 300, "burstime": 290, "priority": 3, "i5": 2, "i3": 3,
     "dualcore": 3, "st": 0, "ct": 0}
sorted_list = sorted(input_list, key=itemgetter('arrival'))
print("sorted_list:")
print(sorted_list)
priority_list = sorted(input_list, key=itemgetter('priority'))
print("priority_list:")
print(priority_list)
priority_list = []
waiting_list=[4,2,6,3,1,5]
allocated=[]
for process in sorted_list:
    for machine in machine_list:
        if machine["processor"]=="i5" and machine_list[0]["idle"]==0:
            if process["i5"]<=(machine["available"]-machine["alloted"]) or process["processno"]==4:
                if process["cores needed"]<=(machine["num core"]*(machine["available"]-machine["alloted"])) or process["processno"]==4:
                    if machine["current"]<process["arrival"]:
                        machine["process"]=machine["process"]+"|idle| p"+str(process["processno"])
                    else:
                        machine["process"]=machine["process"]+"p"+str(process["processno"])
                        machine["alloted"]=machine["alloted"]+process["i5"]
                    if machine[ "alloted" ]>=6:
                            machine["alloted"]=6
                            machine["allocated list"].append(process["processno"])
                            process["st"]= process["arrival"]
                            process["ct"]=process["st"]+ process["burstime"]
                    if process["st"] not in machine["time"]:
                                machine["time"].append(process["st"])
                    if process["ct"] not in machine["time"]:
                                machine["time"].append(process["ct"])
                                machine["current"]= process["ct"]
                                machine[ "idle"]=1
                    if process["processno"] not in allocated:
                                    allocated.append(process["processno"] )
                                    waiting_list.remove(process["processno"] )

                                    break
        elif process ["cores needed"]<=((machine_list[0]["num core"]*(machine_list[0]["available"]- machine_list[0]["alloted"]))+(machine_list[1]["num core"]*(machine_list[1]["available"]-machine_list[1]["alloted"] ) ) ) :
                                    machine_list[0]["process"]=machine_list[0]["process"]+"|idle|p"+str(process["processno"])
                                    machine_list[1]["process"]=machine_list[1]["process"]+"|idle|p"+str(process["processno"])
                                    process["cores needed"]=process["cores needed"]-(machine_list[0]["num core"]*(machine_list[0]["available"]-machine_list[0]["alloted"]))
                                    x=(process["cores needed"]+1)/2
                                    machine_list[0]["alloted"]= machine ["alloted"]+machine_list[0] ["available"]-machine_list[0]["alloted"]
                                    machine_list[1]["alloted"]= machine [ "alloted"]+x
                                    if machine_list[0]["alloted"]>=6:
                                        machine_list[0]["alloted"]=6
                                    if machine_list[1]["alloted"]>=6:
                                        machine_list[1]["alloted"]=6
                                        machine_list[0]["allocated list"].append(process["processno"])
                                        machine_list[1]["allocated list"].append(process["processno"])
                                        process["st"]=process["arrival"]
                                        process[ "ct"]=process["st"]+process["burstime"]
                                    if process["st"] not in machine_list[0]["time"]:
                                        machine_list[0]["time"].append(process["st"])
                                    if process["ct"] not in machine_list[0]["time"]:
                                        machine_list[0]["time"].append(process["ct"])
                                    if process["st"] not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["st"])
                                    if process["ct"] not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["ct"])
                                        machine_list[0]["current"]=process["ct"]
                                        machine_list[2]["current"]=process["ct"]
                                        machine_list[0]["idle"]=1
                                        machine_list[2]["idle"]=1
                                    if process["processno"] not in allocated:
                                        allocated.append(process["processno"])
                                        waiting_list.remove(process["processno"])
                                        break
        elif process["cores needed"]<=((machine_list[0]["num core"]*(machine_list[0]["available"]- machine_list[0]["alloted"]))+(machine_list[2]["num core"]*(machine_list[2]["available"]-machine_list[2]["alloted"]))):
                                    machine_list[0]["process"]=machine_list[0]["process"]+" |idle|p"+str(process["processno"])
                                    machine_list[2]["process"]=machine_list[2]["process"]+"|idle|p"+str(process["processno"])
                                    process["cores needed"]=process["cores needed"]-(machine_list[0]["num core"]*(machine_list[0]["available"]- machine_list[0]["alloted"]))
                                    x=(process["cores needed"]+1)/2
                                    machine_list[0]["alloted"]=machine["alloted"]+machine_list[0]["available"]-machine_list[0]["alloted"]
                                    machine_list[2]["alloted"]=machine["alloted"]+x
                                    if machine_list[0]["alloted"]>=6:
                                        machine_list[0]["alloted"]=6
                                    if machine_list[2]["alloted"]>=6:
                                        machine_list[2]["alloted"]=6
                                        machine_list[0]["allocated list"].append(process["processno"] )
                                        machine_list[2]["allocated list"].append(process[ "processno"] )
                                        process["st"]=process["arrival"]
                                        process["ct"]=process["st"]+process["burstime"]
                                    if process["st"] not in machine_list[0]["time"] :
                                        machine_list[0]["time"].append(process["st"])
                                    if process["ct"] not in machine_list[0]["time"]:
                                        machine_list[0]["time"].append(process["ct"])
                                    if process ["st"] not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["st"])
                                    if  process["ct"]not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["ct"])
                                        machine_list[0]["current"]=process["ct"]
                                        machine_list[2]["current"]=process["ct"]
                                        machine_list[0]["idle"]=1
                                        machine_list[2]["idle"]=1
                                    if process["processno"] not in allocated  :
                                        allocated.append(process["processno"])
                                        waiting_list.remove(process["processno"])
                                        break
                                    
        elif machine["processor"]=="i3"and machine_list[1]["idle"]==0:
                                    if process[ "i3"]<=(machine["available"]-machine["alloted"]):
                                        if process["cores needed"]<=(machine["num core"]*(machine["available"]-machine["alloted"])):
                                            machine["process"]=machine["process"]+"|idle|p"+str(process["processno"] )
                                            machine ["alloted"]=machine["alloted"]+process["i3"]
                                            if machine ["alloted"] >=6:
                                                machine [ "alloted"]=6
                                                machine [ "allocated list"].append(process["processno"])
                                                process["st"]=process["arrival"]
                                                process["ct"]=process[ "st"]+process["burstime"]
                                                machine["current"]=process["ct"]
                                            if process["st"] not in machine ["time"] :
                                                machine["time"].append(process["st"])
                                            if process["ct"] not in machine ["time"] :
                                                machine [ "time"].append(process["ct"])
                                                machine ["idle"]=1
                                            if process["processno"] not in allocated:
                                                allocated.append(process["processno"] )
                                                waiting_list.remove(process["processno"])
                                                break
        elif process["cores needed"]<=((machine_list[1]["num core"]*(machine_list[1]["available"]-machine_list[1]["alloted"]))+(machine_list[2]["num core"]*(machine_list[2]["available"]-machine_list[2]["alloted"]))):
                                    machine_list[1]["process"]=machine_list[1]["process"]+"|idle|p"+str(process["processno"])
                                    machinelist[2]["process"]=machine_list[2]["process"]+"|idle|p"+str(process["processno"])
                                    process["cores needed"]=process["cores needed"]-(machine_list[1]["num core"]*(machine_list[1]["available"]- machine_list[1]["alloted" ]))
                                    x=(process["cores needed"]+1)/2
                                    machine_list[1]["alloted"]=machine["alloted"]+machine_list[1]["available"]-machine_list[1]["alloted" ]
                                    machine_list[2]["alloted"]=machine["alloted"]+x
                                    if machine_list[1]["alloted"]>=6:
                                        machine_list[1]["alloted"]=6
                                    if machine_list[2]["alloted"]>=6:
                                        machine_list[2]["alloted"]=6
                                        machine_list[1]["allocated list"].append(process["processno"])
                                        machine_list[2]["allocated list"].append(process["processno"] )
                                        process["st"]=process["arrival"]
                                        process["ct"]=process["st"]+process["burstime"]
                                    if process["st"] not in machine_list[1]["time"]:
                                        machine_list[1]["time"].append(process["st"])
                                    if process["ct"] not in machine_list[1]["time"]:
                                        machine_list[1]["time"].append(process["ct"] )
                                    if process["st"] not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["st"] )
                                    if process["ct" ] not in machine_list[2]["time"]:
                                        machine_list[2]["time"].append(process["ct"] )
                                        machine_list[1]["current"]=process["ct"]
                                        machine_list[2]["current"]=process["ct"]
                                        machine_list[1]["idle"]=1
                                        machine_list[2]["idle"]=1
                                    if process["processno"] not in allocated:
                                        allocated.append(process["processno"])
                                        waiting_list.remove(process["processno"] )
                                        break
                                